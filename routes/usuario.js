

//Modulos internos

const express = require("express");
const router = express.Router();


//Modulos propios

const { Usuario } = require("../model/Usuario");

// Rutas

router.post("/", async(req, res) =>{

	//Revisar si el usuario existe
	let usuario = await Usuario.findOne({ correo: req.body.correo})

	//Si el usuario existe en la base de datos
	if(usuario) return res.status(400).send("El usuario existe en la base de datos")

	//Si el usuario no existe
	usuario = new Usuario({
		nombre: req.body.nombre,
		correo: req.body.correo,
		password: req.body.password
	})

	//Guardar el usuario en la base de datos y generamos el JWK
	const results = await usuario.save();
	const jwtToken = usuario.generateJWT();
	res.status(200).send({jwtToken});

});


//Modulos Exports

module.exports = router;