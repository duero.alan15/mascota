

//Modulos internos

const express = require("express");
const router = express.Router();


//Modulos propios

const { Usuario } = require("../model/Usuario")

// Rutas

router.post("/", async(req, res) =>{

	//Revisar si el usuario existe
	let usuario = await Usuario.findOne({ correo: req.body.correo})

	//Si el usuario existe en la base de datos
	if(!usuario) return res.status(400).send("Correo o contraseña son invalidos")

	//Si existe el usuario, validamos la contraseña
	if(usuario.password !== req.body.password) return res.status(400).send("Correo o contraseña son invalidos");

	//Si pasa generamos el JWT
	const jwtToken = usuario.generateJWT();
	res.status(200).send({jwtToken});

});

//Exports
module.exports = router;
