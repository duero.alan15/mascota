

//Modulos internos

const express = require("express");
const router = express.Router();


//Modulos propios

const { Usuario } = require("../model/Usuario");
const { Mascota } = require("../model/mascota");

const auth = require("../middleware/auth")

//Rutas
 
router.post ("/" , auth , async (req , res) => {

	//Tomar el usuario si existe
	const usuario = await Usuario.findById(req.usuario._id);

	//Si el usuario no existe
	if(!usuario) return res.status(400).send("El usuario no existe");

	let mascotaExist = await Mascota.findOne({ tipo: req.body.tipo})

	if(mascotaExist) return res.status(400).send("El tipo de mascota ya existe");

	const mascota = new Mascota({

		idUsuario: usuario._id,
		nombre: req.body.nombre,
		tipo: req.body.tipo,
		descripcion: req.body.descripcion
	
	});


	const result = await mascota.save();
	//const jwtToken = tarea.generateJWT();
	//res.status(200).send({jwtToken});
	res.status(200).send(result);

});


//Listar las tareas del usuario


router.get("/Lista", auth, async(req, res) => {

	//Tomar nuestro usuario logueado
	const usuario = await Usuario.findById(req.usuario._id);

	//Si el usuario no existe
	if(!usuario) return res.estatus(400).send("El usuario no existe en la base de datos");

	//Si el usuario existe en la base de datos, tomamos todas sus tareas

	const mascota = await Mascota.find({idUsuario: req.usuario._id});
	res.send(mascota);

});


//Editar las tareas del usuario


router.put("/", auth, async(req, res) => {

	//Tomar nuestro usuario logueado
	const usuario = await Usuario.findById(req.usuario._id);

	//Si el usuario no existe
	if(!usuario) return res.estatus(400).send("El usuario no existe en la base de datos");

	//Si el usuario existe hacemos el update
	const mascota = await Mascota.findByIdAndUpdate(
		req.body._id,
		{
			idUsuario: usuario._id,
			nombre: req.body.nombre,
			tipo: req.body.tipo

		},
		{
			new: true,
		}
	);

	if(!mascota) return res.status(400).send("No hay mascotas registradas");

	//Si existen tareas
	res.status(200).send(mascota);

});

//Eliminar una tarea del usuario registrado

router.delete("/:_id", auth, async(req, res) => {

	//Tomar nuestro usuario logueado
	const usuario = await Usuario.findById(req.usuario._id);

	//Si el usuario no existe
	if(!usuario) return res.estatus(400).send("El usuario no existe en la base de datos");

	const mascota = await Mascota.findByIdAndDelete(req.params._id);
	if(!mascota) return res.status(400).send("No hay tareas asignadas");
	res.status(200).send ( {message:" Mascota Eliminada "} );


})

//Exports
module.exports = router;