

//Modulos Internos

const mongoose = require("mongoose");


//Esquema Mascotas

const esquemaMascotas = new mongoose.Schema({

	idUsuario: String,
	nombre: String,
	tipo: String, 
	descripcion: String

}); 

//Exports

const Mascota = mongoose.model("mascota", esquemaMascotas);

module.exports.Mascota = Mascota;